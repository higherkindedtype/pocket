# Better ZSH History

```shell
HISTFILE="$HOME/.zsh_history"

export HISTSIZE=1200000000
export SAVEHIST=1000000000
setopt APPEND_HISTORY           # Write to history
setopt HIST_EXPIRE_DUPS_FIRST   # Expire duplicate entries first when trimming history.
setopt HIST_FIND_NO_DUPS        # Do not display a line previously found.
setopt HIST_IGNORE_ALL_DUPS     # Delete old recorded entry if new entry is a duplicate.
setopt HIST_IGNORE_DUPS         # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_SPACE        # Don't record an entry starting with a space.
setopt HIST_NO_STORE            # Don't show history commands when invoked
setopt HIST_REDUCE_BLANKS       # Remove superfluous blanks before recording entry.
setopt HIST_SAVE_NO_DUPS        # Older duplicates are omitted.
setopt HIST_VERIFY              # Don't execute immediately upon history expansion.
setopt INC_APPEND_HISTORY       # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY            # Share history between all sessions.
SHELL_SESSION_HISTORY=0         # Disable per-terminal-session
```
