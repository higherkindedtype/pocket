# Commit Related Commands

### Get Commits Between Dates

```bash
git log --after="2018-06-30" --before="2018-07-03" --oneline
```
### Get {author}’s Branches

```bash
git for-each-ref --format=' %(authorname) %09 %(refname)' --sort=authorname | grep '{author}'
```

### Get Commits Ordered by Commit Date Across Branches

```bash
git for-each-ref --sort=-committerdate refs/heads/ \
  --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - \
  %(color:red)%(objectname:short)%(color:reset) - %(contents:subject) - \
  %(authorname) (%(color:green)%(committerdate:relative)%(color:reset))'
```
