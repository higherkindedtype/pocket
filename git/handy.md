# Handy Commands

### Alias for colored diff

Use `icdiff` for colorized diff. First `brew install icdiff`, if `icdiff` is not already installed.

```bash
git config \
  --global alias.clrdiff \
  "difftool --extcmd 'icdiff --highlight --line-numbers --numlines=3 --tabsize=2 --cols=135'"
```

Usage: `git clrdiff`

### Delete Local and Remote Branches

```bash
git checkout <old_name>
git branch -m <new_name>
git push origin -u <new_name>
git push origin --delete <old_name>
```

### Update submodule

```bash
git submodule foreach git pull origin master
```

[See](https://stackoverflow.com/questions/1030169/pull-latest-changes-for-all-git-submodules#1032653) also
