# Commands Reference

| GETTING HELP         |                         |
| -------------------- | ----------------------- |
| `git help command`   | or                      |
| `git command --help` | Show help for a command |

### Repository Creation

| REPOSITORY CREATION |                                               |
| ------------------- | --------------------------------------------- |
| `git init`          | Create a repository in the current directory  |
| `git clone url`     | Clone a remote repository into a subdirectory |

| FILE OPERATIONS                     |                                                |
| ----------------------------------- | ---------------------------------------------- |
| `git add <path>`                    | Add file or files in directory recursively     |
| **`git rm`** `<path>`               | Remove file or directory from the working tree |
| -f                                  | Force deletion of file(s) from disk            |
| **`git mv`** `<path> <destination>` | Move file or directory to new location         |
| -f                                  | Overwrite existing destination files           |
| **git checkout** _\[rev]_ _file_    | Restore file from current branch or revision   |
| -f                                  | Overwrite uncommitted local changes            |

| WORKING TREE              |                                                  |
| ------------------------- | ------------------------------------------------ |
| **git status**            | Show status of the working tree                  |
| **git diff** _\[path]_    | Show diff of changes in the working tree         |
| **git diff** HEAD _path_  | Show diff of stages and unstaged changes         |
| **git add** _path_        | Stage file for commit                            |
| **git reset** HEAD _path_ | Unstage file for commit                          |
| **git commit**            | Commit files that has been staged (with git-add) |
| -a                        | Automatically stage all modified files           |
| **git reset** –soft HEAD^ | Undo commit & keep changes in the working tree   |
| **git reset** –hard HEAD^ | Reset the working tree to the last commit        |
| **git clean**             | Clean unknown files from the working tree        |

| EXAMINING HISTORY:           |                                               |
| ---------------------------- | --------------------------------------------- |
| **git log** _\[path]_        | View commit log, optionally for specific path |
| **git log** _\[from\[..to]]_ | View commit log for a given revision range    |
| --stat                       | List diffstat for each revision               |
| -S’pattern'                  | Search history for changes matching pattern   |
| **git blame** _\[file]_      | Show file annotated with line modifications   |

| REMOTE REPOSITORIES - _REMOTES_:  |                                                  |
| --------------------------------- | ------------------------------------------------ |
| **git fetch** _\[remote]_         | Fetch changes from a remote repository           |
| **git pull** _\[remote]_          | Fetch and merge changes from a remote repository |
| **git push** _\[remote]_          | Push changes to a remote repository              |
| **git remote**                    | List remote repositories                         |
| **git remote add** _remote_ _url_ | Add remote to list of tracked repositories       |

| BRANCHES                         |                                                |
| -------------------------------- | ---------------------------------------------- |
| `git checkout branch`            | Switch working tree to branch                  |
| `git checkout -b <branch>`       | Create branch before switching to it           |
| **git branch**                   | List local branches                            |
| **git branch** -f _branch_ _rev_ | Overwrite existing branch, start from revision |
| **git merge** _branch_           | Merge changes from branch                      |

| EXPORTING AND IMPORTING            |                                              |
| ---------------------------------- | -------------------------------------------- |
| **git apply** - < _file_           | Apply patch from stdin                       |
| **git format-patch** _from\[..to]_ | Format a patch with log message and diffstat |
| **git archive** _rev_ > _file_     | Export snapshot of revision to file          |
| --prefix=_dir_/                    | Nest all files in the snapshot in directory  |
| --format=\*\[tar                   | zip]\*                                       |

| TAGS                      |                                          |
| ------------------------- | ---------------------------------------- |
| `git tag name [revision]` | Create tag for a given revision          |
| -s                        | Sign tag with your private key using GPG |
| -l _\[pattern]_           | List tags, optionally matching pattern   |

| FILE STATUS FLAGS |                                             |
| ----------------- | ------------------------------------------- |
| `M`               | modified - File has been modified           |
| `C`               | copy - File has been copied and modified    |
| `R`               | rename - File has been renamed and modified |
| `A`               | added - File has been added                 |
| `D`               | deleted - File has been deleted             |
| `U`               | unmerged - File has conflicts after a merge |
