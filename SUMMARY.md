# Table of contents

* [🏠 Home](README.md)

## 💻 Bash

* [❓ Conditional Options](bash/conditional-options.md)
* [🗣️ declare](bash/declare.md)
* [🍭 Tidbits](bash/tidbits.md)
* [😬 vim](bash/vim.md)

## GIT

* [Handy Commands](git/handy.md)
* [Commit Related Commands](git/commits.md)
* [Commands Reference](git/reference.md)

## 🌺 CSS

* [CSS Gradients](css/css-gradients.md)
* [Flexbox](css/flexbox.md)
* [Unsorted](css/unsorted.md)

## 🤪 misc

* [Monad Laws](misc/monad-laws.md)
* [SBT - Repeat Test](misc/sbt-repeat-test.md)
* [Maven Install](misc/maven.md)
* [📄 Sublime Text](misc/sublime-text.md)
* [Morse Code](misc/morse-code.md)
