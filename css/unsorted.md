# Unsorted

#### Standard and Handy Media Queries

Refined to work better with 960 grids. [Original source](https://stackoverflow.com/a/7354648)

```css
@media (min-width:320px)  { /* smartphones, iPhone, portrait 480x320 phones */ }
@media (min-width:481px)  { /* portrait e-readers (Nook/Kindle), smaller tablets @ 600 or @ 640 wide. */ }
@media (min-width:641px)  { /* portrait tablets, portrait iPad, landscape e-readers, landscape 800x480 or 854x480 phones */ }
@media (min-width:961px)  { /* tablet, landscape iPad, lo-res laptops ands desktops */ }
@media (min-width:1025px) { /* big landscape tablets, laptops, and desktops */ }
@media (min-width:1281px) { /* hi-res laptops and desktops */ }
```

#### Select specific instances of element

Learnt it from [**here**](https://stackoverflow.com/questions/7084112/css-negation-pseudo-class-not-for-parent-ancestor-elements)

```css
code { /* Applies to all <code> tags */
  font-family: Fira Code, monospace !important;
  font-size: 16px !important;
}

pre code { /* Applies to <code> with <pre> code block */
  line-height: 1.8em;
}

/* Applies to all <code> that is not inside <pre> (code block). Awesome! */
code:not(pre code) {
  font-weight: bold;
  color: #555;
}
```

#### Hand-drawn Border

```css
.hand-drawn-border {  
  border-bottom-left-radius: 15px 255px;
  border-bottom-right-radius: 225px 15px;
  border-top-left-radius: 255px 15px;
  border-top-right-radius: 15px 225px;
}
```
