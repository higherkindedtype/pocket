# CSS Gradients

### Gradient Backgrounds

[linear, radial, duotone](https://codepen.io/EricPorter/pen/yXKGoX)

{% embed url="https://codepen.io/EricPorter/pen/yXKGoX" %}
Gradient Backgrounds
{% endembed %}

### Gradient Spinner

{% embed url="https://codepen.io/AdamDipinto/pen/eYOaGvY" %}
Gradient Spinner
{% endembed %}

### Pink to Yellow Gradient

```html
<div class="bg-gradient-to-r from-pink to-yellow">Hello World</div>
```

```css
.to-yellow {
  --gradient-to-color: #FFBE0B;
}

.from-pink {
  --gradient-from-color: #FF006E;
  --gradient-color-stops: var(--gradient-from-color), var(--gradient-to-color, rgba(255, 0, 110, 0));
}

.bg-gradient-to-r {
  background-image: linear-gradient(to right, var(--gradient-color-stops));
}
```
