# Flexbox

#### Stretch an image to the length of the div

If the `div` spans the entire width of the page, the image stretches too. Ideal for featured image of a blog post. Learnt the trick from [here](https://stackoverflow.com/a/20341542)

```css
.image.featured {
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: hidden
}

.image.featured img {
    flex-shrink: 0;
    min-width: 100%;
    min-height: 100%
}
```
