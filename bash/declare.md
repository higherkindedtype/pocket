# 🗣 declare

### The Command

```bash
declare [-aAfFgilnrtux] [-p] [name[=value] ...]
```

### Options

```bash
-f    restrict action or display to function names and definitions
-F    restrict display to function names only (plus line number and source file when debugging)
-g    create global variables when used in a shell function; otherwise ignored
-p    display the attributes and value of each NAME
```

### Options to set attributes

```bash
-a    make NAMEs indexed arrays (if supported)
-A    make NAMEs associative arrays (if supported)
-i    make NAMEs have the `integer` attribute
-l    convert NAMEs to lower case on assignment
-n    make NAME a reference to the variable named by its value
-r    make NAMEs readonly
-t    make NAMEs have the `trace` attribute
-u    convert NAMEs to upper case on assignment
-x    make NAMEs export
```

#### Note

* Using `+` instead of `-` off the given attribute.
* Variables with the integer attribute have arithmetic evaluation (see the let command) performed when the variable is assigned a value.
* When used in a function, declare makes NAMEs local, as with the local command. The `-g` option suppresses this behavior.

#### Exit Status

Returns success unless an invalid option is supplied or a variable assignment error occurs.
