# ❓ Conditional Options

### Command Grammar

```bash
[[ -<option> <arg> ]] && echo 1 || echo 0

if [[ -<option> <arg> ]]; then
  ...
else
  ...
fi
```

### Conditional Options for Values

```
-n   string is not null
-z   string is null, that is, has zero length
```

### Conditional Options for File/Folder

| Option |      Usage     | Description                                                                                                            |
| :----: | :------------: | ---------------------------------------------------------------------------------------------------------------------- |
|  `-b`  | `[ -b $path ]` | Checks if file is a block special file; if yes, then the condition becomes true.                                       |
|  `-c`  | `[ -c $path ]` | Checks if file is a character special file; if yes, then the condition becomes true.                                   |
|  `-d`  | `[ -d $path ]` | Checks if file is a directory; if yes, then the condition becomes true. `[ -d $file ]` is not true.                    |
|  `-f`  | `[ -f $path ]` | Checks if file is an ordinary file as opposed to a directory or special file; if yes, then the condition becomes true. |
|  `-g`  | `[ -g $path ]` | Checks if file has its set group ID (SGID) bit set; if yes, then the condition becomes true.                           |
|  `-k`  | `[ -k $path ]` | Checks if file has its sticky bit set; if yes, then the condition becomes true.                                        |
|  `-p`  | `[ -p $path ]` | Checks if file is a named pipe; if yes, then the condition becomes true.                                               |
|  `-t`  | `[ -t $path ]` | Checks if file descriptor is open and associated with a terminal; if yes, then the condition becomes true.             |
|  `-u`  | `[ -u $path ]` | Checks if file has its Set User ID (SUID) bit set; if yes, then the condition becomes true.                            |
|  `-r`  | `[ -r $path ]` | Checks if file is readable; if yes, then the condition becomes true.                                                   |
|  `-w`  | `[ -w $path ]` | Checks if file is writable; if yes, then the condition becomes true.                                                   |
|  `-x`  | `[ -x $path ]` | Checks if file is executable; if yes, then the condition becomes true.                                                 |
|  `-s`  | `[ -s $path ]` | Checks if file has size greater than 0; if yes, then condition becomes true.                                           |
|  `-e`  | `[ -e $path ]` | Checks if file exists; is true even if file is a directory but exists.                                                 |
