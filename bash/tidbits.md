# 🍭 Tidbits

#### The Arguments

```bash
echo "argc = ${#*}"
echo "argv = ${*}"
```

#### Template to `find` files and process each file

```bash
find . -iname '*.txt' -type f | while read fname; do
  echo "Reading from $fname. Writing to `basename ${fname%.*}`.bin"
  base64 -D "${fname}" > "${fname%.*}.bin"
done
```

#### Split String by Delimiter

ref: https://stackoverflow.com/a/918931

```bash
function split_it () {
  while IFS="${2:-;}" read -ra ADDR; do
    for i in "${ADDR[@]}"; do
      # process "$i"
    done
  done <<< "$1"
} <<< "$IN"
```
