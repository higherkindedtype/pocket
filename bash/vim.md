# 😬 vim

#### Remapping Keys

```bash
nnoremap <A-S-Up> :m-2<CR>
nnoremap <A-S-Down> :m+<CR>
inoremap <A-S-Up> <Esc>:m-2<CR>
inoremap <A-S-Down> <Esc>:m+<CR>
```
