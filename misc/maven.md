# Maven Install

### Install a local file to the local M2 repo

```bash
mvn install:install-file \
	-Dfile='expressly-5.0.0.jar'
	-Dfile='pom.xml'
	-DgroupId='org.glassfish.expressly' \
	-DartifactId='expressly' \
	-Dversion='5.0.0'
```

### Install a remote JAR on local M2 repo

```bash
mvn org.apache.maven.plugins:maven-dependency-plugin:2.1:get \
	-Dartifact='org.glassfish.expressly:expressly:5.0.0' \
	-DrepoUrl='https://repo.maven.org'

mvn org.apache.maven.plugins:maven-dependency-plugin:2.1:get \
	-Dartifact='jakarta.el:jakarta.el-api:5.0.0' \
	-DrepoUrl='https://repo.maven.org'
```
