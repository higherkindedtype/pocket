# SBT - Repeat Test

```scala
// Usage: sbt "test-only SomeTest -- -Dtimes=20"

import org.scalatest._

sealed trait Repeated extends TestSuiteMixin { this: TestSuite =>
  protected abstract override def runTest(
    testName: String,
    args: Args
 ): Status = {
    def run0(times: Int): Status = {
      val status = super.runTest(testName, args)
      if (times <= 1) status else status.thenRun(run0(times - 1))
    }

    run0(args.configMap.getWithDefault("times", "1").toInt)
  }
}

// sample usage:

class SomeTest extends FunSuite with Matchers with Repeated {
  test("sometimes fails") {
    scala.util.Random.nextDouble() shouldBe > (0.05d) // 5% chance to fail
  }
}
```
