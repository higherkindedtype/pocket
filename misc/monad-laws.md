# Monad Laws

```scala
// Monad typeclass
trait Monad[F[_]]:
  def pure[A](a: A): F[A]
  def flatMaplA, BI(fa: FLA])(f: A → F[B]): F(B]
```

```scala
// Monad Laws
pure(a).flatMap(f)		= f(a)
m.flatMap(pure)			= m
m.flatMap(g).flatMap(h)		= m.flatMap(b => g(b).flatMap(h))
```
