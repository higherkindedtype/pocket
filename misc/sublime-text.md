# 📄 Sublime Text

#### Change the shortcut for pane switching (default: `Ctrl+1...n` )

* `Ctrl+1...n`  to focus on a particular pane (in multi-pane mode)
* ref: [https://superuser.com/a/1666927](https://superuser.com/a/1666927)

```json
{
  "keys": [ "alt+tab" ],
  "command": "focus_neighboring_group",
  "args": { "forward": true }
},
{
  "keys": [ "alt+shift+tab" ],
  "command": "focus_neighboring_group",
  "args": { "forward": false }
}
```
